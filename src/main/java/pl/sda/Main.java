package pl.sda;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int arr[] = {5, 10, 15, 1, 2, 3, 4};
        quickSort(0, arr.length - 1, arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void quickSort(int l, int r, int[] arrayToSort) {
        if (l < r) {
            int partitionIndex = partitionArray(l, r, arrayToSort);
            quickSort(l, partitionIndex - 1, arrayToSort);
            quickSort(partitionIndex + 1, r, arrayToSort);
        }
    }

    private static int partitionArray(int l, int r, int[] arrayToSort) {
        int partitionIndex = selectPartitionIndex(l, r);
        int partitionValue = arrayToSort[partitionIndex];
        swap(arrayToSort, partitionIndex, r);
        int lastSwapped = l;

        for (int i = l; i < r; i++) {
            if (arrayToSort[i] < partitionValue) {
                swap(arrayToSort, i, lastSwapped);
                lastSwapped++;
            }
        }
        swap(arrayToSort, lastSwapped, r);
        return lastSwapped;
    }

    private static void swap(int[] arrayToSort, int a, int b) {
        int tmp = arrayToSort[a];
        arrayToSort[a] = arrayToSort[b];
        arrayToSort[b] = tmp;
    }

    private static int selectPartitionIndex(int l, int r) {
        return (l + r) / 2;
    }
}
